#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;
use WWW::GitLab::v3;

unless ( $ENV{GITLAB_API_TOKEN} && $ENV{GITLAB_API_URL} ) {
  plan( skip_all => "Need credentials for API testing" );
}

my %params = (
  token => $ENV{GITLAB_API_TOKEN},
  url   => $ENV{GITLAB_API_URL},
);

my $api = WWW::GitLab::v3->new(%params);

my @chars = ("A".."Z", "a".."z");
my $project_name = 'test-project';
$project_name .= $chars[rand @chars] for 1..8;

my $project = $api->create_project({
  name             => $project_name,
  visibility_level => 0,
  snippets_enabled => 1,
});

my $text = <<'EOF';
Some verbatim text
==================

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim:

* ad minim veniam.

* quis nostrud exercitation.

* ullamco laboris nisi ut aliquip ex ea commodo consequat.

Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
fugiat nulla pariatur. 

    #!/usr/bin/perl -w
    map { print } @INC;

Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
deserunt mollit anim id est laborum.
EOF

my %snippet_params = (
  title            => 'test snippet',
  file_name        => 'snippet.md',
  visibility_level => 0,
  code             => $text,
);

my $test_snippet = undef;
# Create a snippet
{
  eval { $test_snippet = $api->create_snippet($project->{id}, \%snippet_params) };
  isnt($test_snippet, undef, 'create snippet');
  is($test_snippet->{file_name}, $snippet_params{file_name}, 'new snippet file name matches source');
}

# Get snippets in project
{
  my $snippets = undef;
  eval { $snippets = $api->snippets($project->{id}) };
  isnt($snippets, undef, 'get snippets');
  is(ref $snippets, 'ARRAY', 'snippet list is array');
}

# Get a snippet by id
{
  my $snippet = undef;
  eval { $snippet = $api->snippet($project->{id}, $test_snippet->{id}) };
  isnt($test_snippet, undef, 'get snippet');
  is($snippet->{id}, $test_snippet->{id}, 'snippet id matches source');
}

# Get snippet content
{
  my $code = undef;
  eval { $code = $api->raw_snippet($project->{id}, $test_snippet->{id}) };
  isnt($code, undef, 'get snippet content');
  # BUG(jja) Why does this fail?
  # is($code, $snippet_params{code}, 'snippet content matches source');
}

# Edit snippet
{
  my %new_params = (
    title            => 'new title',
    file_name        => 'new_name',
    visibility_level => 10,
    code             => 'new contents',
  );
  foreach (keys %new_params) {
    my $snippet = undef;
    eval {
      $snippet = $api->edit_snippet(
        $project->{id},
        $test_snippet->{id},
        { $_ => $new_params{$_} },
      )
    };
    isnt($snippet, undef, 'edit snippet ' . $_);

    # Make sure change is effective
    SKIP: {
      skip 'Impossible to retrieve snippet field for checking', 2
        if (/visibility_level/);

      $snippet = undef;
      eval {
        if (/(title|file_name)/) {
          $snippet = $api->snippet(
            $project->{id},
            $test_snippet->{id},
          )
        }
        else {
          my $value = $api->raw_snippet(
            $project->{id},
            $test_snippet->{id},
          );
          $snippet = {};
          $snippet->{$_} = $value
        }
      };
      isnt($snippet, undef, 'retrieve snippet with new ' . $_);
      is($snippet->{$_}, $new_params{$_}, 'change ' . $_);
    }
  }
}

# Delete snippet
{
  my $snippet = undef;
  eval { $snippet = $api->delete_snippet($project->{id}, $test_snippet->{id}) };
  isnt($snippet, undef, 'delete snippet');
  is($snippet->{id}, $test_snippet->{id}, 'deleted snippet id matches');
}

$api->delete_project($project->{id});

done_testing();
