#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;
use WWW::GitLab::v3;

unless ( $ENV{GITLAB_API_TOKEN} && $ENV{GITLAB_API_URL} ) {
    plan( skip_all => "Need credentials for API testing" );
}

my %params = (
    token => $ENV{GITLAB_API_TOKEN},
    url   => $ENV{GITLAB_API_URL},
);

my $api = WWW::GitLab::v3->new(%params);

my $sample_text = <<'EOF';
Some verbatim text
==================

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim:

* ad minim veniam.

* quis nostrud exercitation.

* ullamco laboris nisi ut aliquip ex ea commodo consequat.

Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
fugiat nulla pariatur.

    #!/usr/bin/perl -w
    map { print } @INC;

Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
deserunt mollit anim id est laborum.
EOF

sub random_string {
    my $length = shift;
    my @chars = ("A".."Z", "a".."z");
    my $string = '';
    $string .= $chars[rand @chars] for 1..$length;
    return $string;
}

my $project;
eval {
    # Create a test project
    $project = $api->create_project({
        name             => 'test-project-' . random_string(8),
        visibility_level => 0,
    });

    # Populate the test project
    $api->create_file($project->{id}, {
        file_path => 'readme.md',
        branch_name => 'master',
        content => $sample_text,
        commit_message => 'First commit',
    });
};

plan skip_all => "There were errors in the test setup" if $@;

# Create a tag
{
    my $tag = undef;
    eval {
        $tag = $api->create_tag($project->{id}, {
          tag_name => 'no-message',
          ref => 'master',
        })
    };
    isnt($tag, undef, 'create tag');
}

eval {
    $api->create_file($project->{id}, {
        file_path => 'doc/readme.md',
        branch_name => 'master',
        content => $sample_text,
        commit_message => 'Second commit',
    });
    $api->create_tag($project->{id}, {
        tag_name => 'with-message',
        ref => 'master',
        message => random_string(10),
    });
};

plan skip_all => "There were errors in the test setup" if $@;

my $tag;
# List tags
{
    my $tags = undef;
    eval { $tags = $api->tags($project->{id}) };
    isnt($tags, undef, 'get list of tags');
    is(ref $tags, 'ARRAY', 'tag list is an array');
    $tag = $tags->[0];
}

my $blob;
# Get project tree
{
    my $tree = undef;
    eval { $tree = $api->tree($project->{id}) };
    isnt($tree, undef, 'get project tree');
    is(ref $tree, 'ARRAY', 'project tree is an array');
    $blob = $tree->[1];
}

# Get blob by path
{
    my $content = undef;
    eval {
        $content = $api->blob(
            $project->{id},
            $tag->{commit}->{id},
            { filepath => 'doc/readme.md' },
        )
    };
    isnt($content, undef, 'get blob by path');
    is($content, $sample_text, 'blob matches source');
}

# Get raw blob
{
    my $content = undef;
    eval {
        $content = $api->raw_blob(
            $project->{id},
            $blob->{id},
        )
    };
    use Data::Dumper;
    isnt($content, undef, 'get raw blob') or
      diag(Dumper($blob));
    is($content, $sample_text, 'raw blob matches source');
}

# Get project contributor list
{
    my $list = undef;
    eval { $list = $api->contributors($project->{id}) };
    isnt($list, undef, 'get contributor list');
    is(ref $list, 'ARRAY', 'contributor list is array');
}

# Compare two commits
{
    my $comparison = undef;
    my $tags;
    eval { $tags = $api->tags($project->{id}) };

    SKIP: {
        skip "There were errors on test setup", 1, unless defined $tags;
        eval {
            $comparison = $api->compare($project->{id}, {
                from => $tags->[0]->{commit}->{id},
                to => $tags->[1]->{commit}->{id},
            })
        };
        isnt($comparison, undef, 'get contributor list');
        # NOTE(jja) How does a correct comparison look like?
    }
}

# Get project archive
{
    my $archive = undef;
    eval { $archive = $api->archive($project->{id}) };
    use Data::Dumper;
    isnt($archive, undef, 'get project archive')
      or diag(Dumper($archive));
    # NOTE(jja) We should check that this is a valid archive. Contents as well?
}

# Delete test project
$api->delete_project($project->{id});

done_testing();
