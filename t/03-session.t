#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;
use WWW::GitLab::v3;

unless ( $ENV{GITLAB_API_TOKEN} && $ENV{GITLAB_API_URL} ) {
  plan( skip_all => "Need credentials for API testing" );
}

my %params = (
  token => $ENV{GITLAB_API_TOKEN},
  url   => $ENV{GITLAB_API_URL},
);

my $api = WWW::GitLab::v3->new(%params);

SKIP: {
  skip "Set GITLAB_API_EMAIL and GITLAB_API_PASSWORD to test login", 5
    unless $ENV{GITLAB_API_EMAIL} && $ENV{GITLAB_API_PASSWORD};
  # Login with email and password
  {
    my $session = undef;
    eval {
      $session = $api->session({
        email    => $ENV{GITLAB_API_EMAIL},
        password => $ENV{GITLAB_API_PASSWORD},
      })
    };
    isnt($session, undef, "start session with email and password");
    is($session->{email}, $ENV{GITLAB_API_EMAIL}, "session email matches email");
  }

  # New with email and password
  {
    my $api = undef;
    eval {
      $api = WWW::GitLab::v3->new({
        email    => $ENV{GITLAB_API_EMAIL},
        password => $ENV{GITLAB_API_PASSWORD},
        url      => $ENV{GITLAB_API_URL},
      })
    };
    isnt($api, undef, "create new instance with email and password");
    isa_ok($api, 'WWW::GitLab::v3', 'instance is object of right class');
    is($api->{token}, $ENV{GITLAB_API_TOKEN}, "instance token matches token");
  }
}

SKIP: {
  skip "Set GITLAB_API_LOGIN and GITLAB_API_PASSWORD to test login", 5
    unless $ENV{GITLAB_API_LOGIN} && $ENV{GITLAB_API_PASSWORD};
  # Login with login and password
  {
    my $session = undef;
    eval {
      $session = $api->session({
        login    => $ENV{GITLAB_API_LOGIN},
        password => $ENV{GITLAB_API_PASSWORD},
      })
    };
    isnt($session, undef, "start session with login and password");
    is($session->{name}, $ENV{GITLAB_API_LOGIN}, "session name matches login");
  }

  # New with login and password
  {
    my $api = undef;
    eval {
      $api = WWW::GitLab::v3->new({
        login    => $ENV{GITLAB_API_LOGIN},
        password => $ENV{GITLAB_API_PASSWORD},
        url      => $ENV{GITLAB_API_URL},
      })
    };
    isnt($api, undef, "create new instance with login and password");
    is($api->{token}, $ENV{GITLAB_API_TOKEN}, "instance token matches token");
    isa_ok($api, 'WWW::GitLab::v3', 'instance is object of right class');
  }
}

done_testing();
