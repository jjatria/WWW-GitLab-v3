#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;
use WWW::GitLab::v3;

unless ( $ENV{GITLAB_API_TOKEN} && $ENV{GITLAB_API_URL} ) {
    plan( skip_all => "Need credentials for API testing" );
}

my %params = (
    token => $ENV{GITLAB_API_TOKEN},
    url   => $ENV{GITLAB_API_URL},
);

my $api = WWW::GitLab::v3->new(%params);

sub random_string {
    my $length = shift;
    my @chars = ("A".."Z", "a".."z");
    my $string = '';
    $string .= $chars[rand @chars] for 1..$length;
    return $string;
}

my $project;
eval {
    # Create a test project
    $project = $api->create_project({
        name             => 'test-project-' . random_string(8),
        visibility_level => 0,
    });

    # Populate the test project
    $api->create_file($project->{id}, {
        file_path => 'readme.md',
        branch_name => 'master',
        content => random_string(10),
        commit_message => 'First commit',
    });

    my $file = {
      commit_message => 'edit file',
      content => random_string(10),
      file_path   => 'readme.md',
      branch_name => 'master',
    };

    $api->edit_file($project->{id}, $file);

    $api->create_tag($project->{id}, {
        tag_name => 'second',
        ref => 'master',
    })
};

plan skip_all => "There were errors in the test setup" if $@;

# Get branches
{
    my $branches = undef;
    eval {
        $branches = $api->branches($project->{id})
    };
    isnt($branches, undef, "get branches");
}

# Get branch
{
    my $branch = undef;
    eval {
        $branch = $api->branch($project->{id}, 'master')
    };
    isnt($branch, undef, "get branche by name");
}

# Unprotect branch
{
    my $branch = undef;
    eval {
        $branch = $api->unprotect_branch($project->{id}, 'master')
    };
    isnt($branch, undef, 'unprotect branch');
    is($branch->{protected}, 0, 'branch is unprotected');
}

# Protect branch
{
    my $branch = undef;
    eval {
        $branch = $api->protect_branch($project->{id}, 'master')
    };
    isnt($branch, undef, 'protect branch');
    is($branch->{protected}, 1, 'branch is protected');
}

TODO: {
    local $TODO = 'Receive a 405 error: method not allowed. API bug?';

    # Create branch
    {
        my $branch = undef;
        eval {
            $branch = $api->create_branch($project->{id}, {
                branch_name => 'devel',
                ref => 'master',
            })
        };
        isnt($branch, undef, 'create branch');
        is(ref $branch, 'HASH', 'branch is hash');
    }

    # Delete branch
    {
        my $success = undef;
        eval {
            $success = $api->delete_branch($project->{id}, 'devel')
        };
        isnt($success, undef, 'delete branch');
    }
}

$api->delete_project($project->{id});

done_testing();
