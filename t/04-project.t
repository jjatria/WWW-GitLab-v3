#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;
use WWW::GitLab::v3;

unless ( $ENV{GITLAB_API_TOKEN} && $ENV{GITLAB_API_URL} ) {
  plan( skip_all => "Need credentials for API testing" );
}

my %params = (
  token => $ENV{GITLAB_API_TOKEN},
  url   => $ENV{GITLAB_API_URL},
);

my $api = WWW::GitLab::v3->new(%params);

my $test_project = undef;
my @chars = ("A".."Z", "a".."z");
my $project_name = 'test-project-';
$project_name .= $chars[rand @chars] for 1..8;
my %test_project_params = (
  name                   => $project_name,
  visibility_level       => 0,
);

my $user = $api->current_user;

# Create new project
{
  eval { $test_project = $api->create_project( \%test_project_params ) };
  isnt($test_project, undef, "create project for current user");
  is($test_project->{name}, $test_project_params{name}, "new project name matches");
}

# Get current user projects
{
  my $projects = undef;
  eval { $projects = $api->projects() };
  isnt($projects, undef, "get current user projects");
  is(ref $projects, 'ARRAY', "project list is array");
}

# Get current user owned projects
{
  my $projects = undef;
  eval { $projects = $api->owned_projects() };
  isnt($projects, undef, "get current user's owned projects");
  is(ref $projects, 'ARRAY', "owned project list is array");
}

# Get single project by id
{
  my $project = undef;
  eval { $project = $api->project( $test_project->{id} ) };
  isnt($project, undef, "get projects by id");
  is($project->{id}, $test_project->{id}, "project by id matches test project");
}

# Get project members
{
  my $members = undef;
  eval { $members = $api->project_members( $test_project->{id} ) };
  isnt($members, undef, "get project members");
  is(ref $members, 'ARRAY', "project member list is array");
}

# Get project events
{
  my $events = undef;
  eval { $events = $api->project_events( $test_project->{id} ) };
  isnt($events, undef, "get project events");
  is(ref $events, 'ARRAY', "project event list is array");
}

# Get single project member
{
  my $member = undef;
  eval { $member = $api->project_member( $test_project->{id}, $user->{id} ) };
  isnt($member, undef, "get single project members");
  is(ref $member, 'HASH', "single project member list is hash");
  is($member->{id}, $user->{id}, "single project member id matches user");
}

SKIP: {
  skip "Current user does not have admin rights", 2 unless $user->{is_admin};

  # Get all projects
  {
    my $projects = undef;
    eval { $projects = $api->all_projects() };
    isnt($projects, undef, "get all projects");
    is(ref $projects, 'ARRAY', "all project list is array");
  }
}

# Delete project
{
  my $success = 0;
  eval { $success = $api->delete_project( $test_project->{id} ) };
  is($success, 1, "delete project");
}

# Cannot delete same project twice
{
  my $success = undef;
  eval { $success = $api->delete_project( $test_project->{id} ) };
  isnt($success, 1, "cannot delete project twice");
}

done_testing();
