#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;
use WWW::GitLab::v3;

unless ( $ENV{GITLAB_API_TOKEN} && $ENV{GITLAB_API_URL} ) {
  plan( skip_all => "Need credentials for API testing" );
}

my %params = (
  token => $ENV{GITLAB_API_TOKEN},
  url   => $ENV{GITLAB_API_URL},
);

my $api = WWW::GitLab::v3->new(%params);

my @chars = ("A".."Z", "a".."z");
my $project_name = 'test-project-';
$project_name .= $chars[rand @chars] for 1..8;

my $project = $api->create_project({
  name                   => $project_name,
  visibility_level       => 0,
});

my $sample_text = <<'EOF';
Some verbatim text
==================

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim:

* ad minim veniam.

* quis nostrud exercitation.

* ullamco laboris nisi ut aliquip ex ea commodo consequat.

Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
fugiat nulla pariatur.

    #!/usr/bin/perl -w
    map { print } @INC;

Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
deserunt mollit anim id est laborum.
EOF

my $test_file = {
  file_path   => 'readme.md',
  branch_name => 'master',
};
# Create a file
{
  my %file = %{$test_file};
  $file{commit_message} = 'create file';
  $file{content} = 'file content';
  my $file = \%file;

  my $response = undef;
  eval { $response = $api->create_file($project->{id}, $file) };
  isnt($response, undef, 'create a file');
  is_deeply($response, $test_file, 'created file matches source');
}

# Edit a file
{
  my %file = %{$test_file};
  $file{commit_message} = 'edit file';
  $file{content} = $sample_text;
  my $file = \%file;

  my $response = undef;
  eval { $response = $api->edit_file($project->{id}, $file) };
  isnt($response, undef, 'edit a file');
  is_deeply($response, $test_file, 'edited file matches source');
}

# Get a file
{
  my $file = {
    ref       => $test_file->{branch_name},
    file_path => $test_file->{file_path},
  };

  my $response = undef;
  eval { $response = $api->file($project->{id}, $file) };
  isnt($response, undef, 'get a file');
  is($response->{ref}, $file->{ref}, 'received file ref matches source');
  is($response->{file_path}, $file->{file_path}, 'received file path matches source');

  use MIME::Base64;
  is( decode_base64($response->{content}), $sample_text, 'received file content matches source');
}

# Delete a file
{
  my %file = %{$test_file};
  $file{commit_message} = 'delete file';
  my $file = \%file;

  my $response = undef;
  eval { $response = $api->delete_file($project->{id}, $file) };
  isnt($response, undef, 'delete a file');
  is_deeply($response, $test_file, 'deleted file matches source');
}

$api->delete_project($project->{id});

done_testing();
