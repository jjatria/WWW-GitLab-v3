#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;
use WWW::GitLab::v3;

unless ( $ENV{GITLAB_API_TOKEN} && $ENV{GITLAB_API_URL} ) {
  plan( skip_all => "Need credentials for API testing" );
}

my %params = (
  token => $ENV{GITLAB_API_TOKEN},
  url   => $ENV{GITLAB_API_URL},
);

my $api = WWW::GitLab::v3->new(%params);

# Get current user
my $user = undef;
{
  eval { $user = $api->current_user };
  isnt($user, undef, "get current user");
  is($user->{private_token}, $params{token}, "current user has same token");
  is(exists $user->{name}, 1, "current user has name");
  is(exists $user->{id}, 1, "current user has id");
}

# Get user list
{
  my $users = undef;
  eval { $users = $api->users };
  isnt($users, undef, "get list of users");
  is(ref $users, 'ARRAY', "user list is array");
}

# Search in user list
{
  my $users = undef;
  eval { $users = $api->users({ search => $user->{name}} ) };
  isnt($users, undef, "search users by name");
  is($users->[0]->{id}, $user->{id}, "got current user by name");
}

# Get single user
{
  my $remote = undef;
  eval { $remote = $api->user($user->{id}) };
  isnt($remote, undef, "get single user by id");
  is($remote->{id}, $user->{id}, "got current user by id");
}

my $test_key = undef;
my %test_key_params = (
  title => 'testkey',
  key => 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCweYn9KVvGww6DZfyLdhXDIknHkXw0itZQlwXrItAHJpH2S/XsJRZInHfjwP2J00xkyiMzzS0Cke0djx0LXaDCFOkhX2n3n94RWGZl9D+M8eYs0l4lKbvHcGRZTgNdIykWBx5NjHCCBUlUbH1oFLJ1hbuqtJFgQiz7bTCQ9U961n6BUa5GpCmgmTQL7CMyqytAiAP6OwNxUf0EgJcXUnxsLY3sGsVx+CgIDaa7/+AfckDFiAs/ykxHPdsiNKCP4F9gdkzkjf8HPA6yP6YgszezuotNoPMG4dwrZ7jH+vKbI9wiyk4RMEOJNcLu6ZJStpMHPCa3wgkcO2WxUEAidhTr test@key',
);
# Create ssh key for current user
{
  eval { $test_key = $api->create_current_user_ssh_key(\%test_key_params) };
  isnt($test_key, undef, "create ssh key for current user");
  is(exists $test_key->{id}, 1, "created key has id field");
}

# Find specific key
{
  my $key = undef;
  eval { $key = $api->user_ssh_key( $test_key->{id} ) };
  isnt($key, undef, "get key by id");
  is(exists $key->{id}, 1, "key has id field");
  is_deeply($test_key, $key, "local and remote keys match");
}

# Create existing ssh key for current user
{
  my $key = undef;
  eval { $key = $api->create_current_user_ssh_key(\%test_key_params) };
  is($key, undef, "cannot create existing key");
}

# List ssh keys for current user
{
  my $keys = undef;
  eval { $keys = $api->current_user_ssh_keys() };
  isnt($keys, undef, "get ssh keys for user");
  is(ref $keys, 'ARRAY', "ssh keys for user is array");
}

# Delete ssh key for current user
{
  my $deleted_key = undef;
  eval { $deleted_key = $api->delete_current_user_ssh_key($test_key->{id}) };
  isnt($deleted_key, undef, "delete ssh for current user");
  # Ignore following fields when comparing
  is($deleted_key->{key}, $test_key->{key}, "local and deleted keys match");

  my $key = undef;
  eval { $key = $api->user_ssh_key($test_key->{id}) };
  is($key, undef, "deleted key no longer exists");
}

# Create ssh key for same user by id
{
  eval {
    $test_key = $api->create_user_ssh_key($user->{id}, \%test_key_params)
  };
  isnt($test_key, undef, "create ssh key for same user by id");
  is(exists $test_key->{id}, 1, "created key for same user with id has id");
}

# Delete ssh key for same user by id
{
  eval {
    $test_key = $api->delete_user_ssh_key($user->{id}, $test_key->{id})
  };
  isnt($test_key, undef, "delete ssh key for same user by id");
  is(exists $test_key->{id}, 1, "deleted key for same user with id has id");
}

## Admin methods

SKIP: {
  skip "Current user does not have admin rights", 1 unless $user->{is_admin};

  my $test_user;
  # Create user
  {
    my %params = (
      email    => 'a.test.email@example.com',
      password => 'passtest',
      name     => 'A Test User',
      username => 'apiusertest',
    );

    my $users;
    $users = $api->users( { search => $params{email} } );

    my $before = $#{$users};
    $api->create_user( \%params );

    $users = $api->users( { search => $params{email} } );
    my $after = $#{$users};

    is($before, $after - 1);

    $test_user = shift @{$users};
  }

  # Edit user
  {
    my %params = (
      name => 'A New User Name',
    );

    $api->edit_user($test_user->{id}, \%params);
    $test_user = $api->user($test_user->{id});

    is($test_user->{name}, $params{name});
  }

  TODO: {
    local $TODO = "Need access to admin user to figure out test";

    # Create ssh key for different user
    {
      eval {
        $test_key = $api->create_user_ssh_key($user->{id}, \%test_key_params)
      };
      isnt($test_key, undef, "create key for different user");
      is(exists $test_key->{id}, 1, "created key for different user has id");
    }
  };


  # Delete user
  {
    $api->delete_user($test_user->{id});
    $test_user = undef;
    eval { $test_user = $api->user($test_user->{id}) };

    is($test_user, undef);
  }
}

# Attempt to create key with wrong parameters
{
  $test_key = undef;
  my %bad_key_params = %test_key_params;
  $bad_key_params{wrong_field} = 'wrong value';
  eval { $test_key = $api->create_current_user_ssh_key(\%bad_key_params) };
  is($test_key, undef, "parameter check for current user key creation");
  ok(!exists $test_key_params{wrong_field}, 'key does not exist');
}

done_testing();
