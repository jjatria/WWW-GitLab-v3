#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;
use WWW::GitLab::v3;

unless ( $ENV{GITLAB_API_TOKEN} && $ENV{GITLAB_API_URL} ) {
    plan( skip_all => "Need credentials for API testing" );
}

my %params = (
    token => $ENV{GITLAB_API_TOKEN},
    url   => $ENV{GITLAB_API_URL},
);

my $api = WWW::GitLab::v3->new(%params);

sub random_string {
    my $length = shift;
    my @chars = ("A".."Z", "a".."z");
    my $string = '';
    $string .= $chars[rand @chars] for 1..$length;
    return $string;
}

my $project;
my $tag;
eval {
    # Create a test project
    $project = $api->create_project({
        name             => 'test-project-' . random_string(8),
        visibility_level => 0,
    });

    # Populate the test project
    $api->create_file($project->{id}, {
        file_path => 'readme.md',
        branch_name => 'master',
        content => random_string(10),
        commit_message => 'First commit',
    });

    my $file = {
      commit_message => 'edit file',
      content => random_string(10),
      file_path   => 'readme.md',
      branch_name => 'master',
    };

    $api->edit_file($project->{id}, $file);

    $tag = $api->create_tag($project->{id}, {
        tag_name => 'second',
        ref => 'master',
    })
};

plan skip_all => "There were errors in the test setup" if $@;

# Get commits
{
  my $commits = undef;
  eval { $commits = $api->commits( $project->{id} ) };
  isnt($commits, undef, 'get commits');
  is(ref $commits, 'ARRAY', 'commit list is array');
}

# Get commits from ref
{
  my $commits = undef;
  eval { $commits = $api->commits( $project->{id}, { ref_name => 'master' } ) };
  isnt($commits, undef, 'get commits from ref');
  is(ref $commits, 'ARRAY', 'commit list from ref is array');
}

# Get commit by id
{
  my $commit = undef;
  eval {
      $commit = $api->commit( $project->{id}, $tag->{commit}->{id} )
  };
  isnt($commit, undef, 'get commit by id');
  is(ref $commit, 'HASH', 'commit by id is hash');
  is(defined $commit->{id}, 1, 'commit by id has id');
  is($commit->{id}, $tag->{commit}->{id}, 'commit by id matches first tag');
}

# Commit diff
{
  my $diff = undef;
  eval {
      $diff = $api->commit_diff( $project->{id}, $tag->{commit}->{id} )
  };
  isnt($diff, undef, 'get commit diff');
  is(ref $diff, 'ARRAY', 'commit diff returns array');
  is(ref $diff->[0], 'HASH', 'commit diff is array of hashes');
  is(defined $diff->[0]->{diff}, 1, 'commit diff has diff');
}

# Add commit comment
{
  my $comment = undef;
  eval {
      $comment = $api->add_commit_comment(
          $project->{id},
          $tag->{commit}->{id},
          {
            note      => 'The text of my comment',
            path      => 'readme.md',
            line      => 3,
            line_type => 'new',
          }
      )
  };
  isnt($comment, undef, 'post comment on commit');
  is(ref $comment, 'HASH', 'comment is hash');
  is(defined $comment->{note}, 1, 'comment has note');
  is($comment->{note}, 'The text of my comment', 'comment matches source');
  TODO: {
    local $TODO = "API doesn't seem to allow posting line comments";
    is($comment->{line}, 3, 'comment line matches source');
    is($comment->{line_type}, 'new', 'comment line_type matches source');
  }
  TODO: {
    local $TODO = "API seems to ignore path";
    is($comment->{path}, 'readme.md', 'comment path matches source');
  }
}

# Add commit comment
{
  my $comments = undef;
  eval {
      $comments = $api->commit_comments(
          $project->{id},
          $tag->{commit}->{id},
      )
  };
  isnt($comments, undef, 'get comment on commit');
  is(ref $comments, 'ARRAY', 'comment list is array');
  is(ref $comments->[0], 'HASH', 'comment list is aray of hashes');
  is(defined $comments->[0]->{note}, 1, 'comment from list has note');
  is($comments->[0]->{note}, 'The text of my comment', 'comment from list matches source');
}

$api->delete_project($project->{id});

done_testing();
