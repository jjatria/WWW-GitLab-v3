#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;
use WWW::GitLab::v3;

unless ( $ENV{GITLAB_API_TOKEN} && $ENV{GITLAB_API_URL} ) {
  plan( skip_all => "Need credentials for API testing" );
}

my %params = (
  token => $ENV{GITLAB_API_TOKEN},
  url   => $ENV{GITLAB_API_URL},
);

my $api;
eval { $api = WWW::GitLab::v3->new(%params) };

isnt($api, undef, 'constructor works');
isa_ok($api, 'WWW::GitLab::v3');

done_testing();
