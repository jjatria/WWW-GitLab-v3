# NAME

WWW::GitLab::v3 - A lightweight and complete GitLab API v3 interface.

# VERSION

Version 0.01

# SYNOPSIS

    use WWW::GitLab::v3;
    my $api = WWW::GitLab::v3->new(
      url   => $v3_api_url,
      token => $token,
    );
    my $branches = $api->branches( $project_id );

# EXPORT

WWW::GitLab::v3 exports nothing.

# DESCRIPTION

This module tries to provide the most lightweight interface to the GitLab API
possible.

The interface is modelled after GitLab::API::v3, and in most cases the two
modules should be interchangeable. The biggest difference is probably in the
parameter validation and the return value of some methods: while GitLab::API::v3
claims to closely follow the return values specified in the specs of the API, there are cases in which this is not strictly the case, and other cases in which
requests to the API do return values, even when the spec is vague.

When a request is sent to the GitLab API, WWW::GitLab::v3 captures the response
and attempts to read it as JSON (except when files or archives are requested).
When the serialization is successful, the serialized data is returned. When it
isn't (or when it is not applicable) the return value is whatever was received 
from the API.

All requests are done using LWP::UserAgent, and a failed request of any
kind will cause a call to `croak`. You might want to try and catch these on
your own with `Try::Tiny`.

Much of this documentation has been borrowed from that of GitLab::API::v3, but
changes have been made where differences exist (so prefer this one when dealing
with this module). For more detail, check the GitLab documentation.

## LOGGING

This module's constructor accepts one optional `debug` argument that prints the
request URL prior to the actual request being sent. This is all the logging
you'll get from this module.

# REQUIRED ARGUMENTS

## url

The URL to your v3 API endpoint. Typically this will be something like
`http://git.example.com/api/v3`.

# CREDENTIALS

The WWW::GitLab::v3 constructor expects some way to authenticate, and will
croak unless this is provided, or possible to obtain. The way this is
implemented is with a private token for the authenticated user, which gets
passed with the various requests to the API.

If this token is not passed to the constructor together with the API url, then
the constructor will expect a valid user email or login name, and their
password. Any of these combinations is accepted.

## token

A GitLab API token.

## login

A GitLab user login name, needed if no token is provided.

## email

A GitLab user email, needed if no login is provided.

## password

A GitLab user password, needed if no token is provided.

# OPTIONAL ARGUMENTS

## debug

A boolean to enable printing the request URLs before calling them. Off by 
default. If set, the requests will be printed to STDERR.

# USER METHODS

See [http://doc.gitlab.com/ce/api/users.html](http://doc.gitlab.com/ce/api/users.html).

## users

    my $users = $api->users(
      \%params,
    );

Sends a `GET` request to `/users` and returns the decoded/deserialized response body. Possible parameters are:

- `page`=NUMBER (optional)
- `per_page`=NUMBER (optional)
- `search`=EMAIL,USERNAME (optional)

## user

    my $user = $api->user(
      $user_id,
    );

Sends a `GET` request to `/users/:user_id` and returns the
decoded/deserialized response body.

## create\_user

    $api->create_user(
      \%params,
    );

Sends a `POST` request to `/users`. This method requires admin rights.
Possible parameters are:

- `email` (required)
- `username` (required)
- `password` (required)
- `name` (required)
- `password` (optional)
- `skype` (optional)
- `linkedin` (optional)
- `twitter` (optional)
- `website_url` (optional)
- `projects_limit` (optional)
- `extern_uid` (optional)
- `provider` (optional)
- `bio` (optional)
- `admin`=TRUE,FALSE (optional)
- `can_create_groups`=TRUE,FALSE (optional)

## edit\_user

    $api->edit_user(
      $user_id,
      \%params,
    );

Sends a `PUT` request to `/users/:user_id`. This method requires admin rights.
Possible parameters are the same as those for `create_user`, but all are
optional.

## delete\_user

    my $user = $api->delete_user(
      $user_id,
    );

Sends a `DELETE` request to `/users/:user_id` and returns the serialised 
deleted user on success.

## current\_user

    my $user = $api->current_user();

Sends a `GET` request to `/user` and returns the decoded/deserialized response body.

## current\_user\_ssh\_keys

    my $keys = $api->current_user_ssh_keys();

Sends a `GET` request to `/user/keys` and returns the decoded/deserialized response body.

## user\_ssh\_keys

    my $keys = $api->user_ssh_keys(
      $user_id,
    );

Sends a `GET` request to `/users/:user_id/keys` and returns the decoded/deserialized response body.

## user\_ssh\_key

    my $key = $api->user_ssh_key(
      $key_id,
    );

Sends a `GET` request to `/user/keys/:key_id` and returns the decoded/deserialized response body.

## create\_current\_user\_ssh\_key

    my $created_key = $api->create_current_user_ssh_key(
      \%params,
    );

Sends a `POST` request to `/user/keys` and returns the decoded/deserialized response body. Possible parameters are:

- `title` (required)
- `key` (required)

## create\_user\_ssh\_key

    $api->create_user_ssh_key(
      $user_id,
      \%params,
    );

Sends a `POST` request to `/users/:user_id/keys`. This method requires admin 
rights. Possible parameters are the same as those for
`create_current_user_ssh_key`.

## delete\_current\_user\_ssh\_key

    my $deleted_key = $api->delete_current_user_ssh_key(
      $key_id,
    );

Sends a `DELETE` request to `/user/keys/:key_id` and returns the decoded/deserialized response body. This mehod requires admin rights.

## delete\_user\_ssh\_key

    $api->delete_user_ssh_key(
      $user_id,
      $key_id,
    );

Sends a `DELETE` request to `/users/:user_id/keys/:key_id`.

# SESSION METHODS

See [http://doc.gitlab.com/ce/api/session.html](http://doc.gitlab.com/ce/api/session.html).

## session

    my $authenticated_user = $api->session(
      \%params,
    );

Sends a `POST` request to `/session` and returns the decoded/deserialized 
response body. Possible optional parameters are:

- _login_ (required)
- _email_ (required if `login` not provided)
- _password_ (required)

# PROJECT METHODS

See [http://doc.gitlab.com/ce/api/projects.html](http://doc.gitlab.com/ce/api/projects.html).

## projects

    my $projects = $api->projects(
      \%params,
    );

Sends a `GET` request to `/projects` and returns the decoded/deserialized response body. Possible optional parameters are:

- `archived`=TRUE,FALSE
- `order_by`=`id`, `name`, `path`, `created_at`, `updated_at`,
`last_activity_at`
- `sort`=`asc`,`desc`
- `search`=`asc`,`desc`

## owned\_projects

    my $projects = $api->owned_projects(
      \%params,
    );

Sends a `GET` request to `/projects/owned` and returns the decoded/deserialized response body. Possible optional parameters are the same as for `projects()`:

## all\_projects

    my $projects = $api->all_projects();

Sends a `GET` request to `/projects/all` and returns the decoded/deserialized response body. Possible optional parameters are the same as for `projects()`:

## project

    my $project = $api->project(
      $project_id,
    );

Sends a `GET` request to `/projects/:project_id` and returns the decoded/deserialized response body.

## project\_events

    my $events = $api->project_events(
      $project_id,
    );

Sends a `GET` request to `/projects/:project_id/events` and returns the decoded/deserialized response body.

## create\_project

    my $project = $api->create_project(
      \%params,
    );

Sends a `POST` request to `/projects` and returns the decoded/deserialized response body. Possible parameters are:

- `name` (required)
- `path` (optional)
- `namespace_id` (optional)
- `issues_enabled` (optional)
- `merge_requests_enabled` (optional)
- `wiki_enabled` (optional)
- `snippets_enabled` (optional)
- `public` (optional)
- `visibility_level` (optional)
- `import_url` (optional)

## create\_project\_for\_user

    $api->create_project_for_user(
      $user_id,
      \%params,
    );

Sends a `POST` request to `/projects/user/:user_id`. This method requires admin 
rights. Possible parameters are the same as for `create_project()`.

## edit\_project

    my $edited_project = $api->edit_project(
      $project_id,
      \%params,
    );

Sends a `PUT` request to `/projects/:project_id` and returns the
decoded/deserialized response body. Possible parameters are the same as for
`create_project()`, but all are optional.

## fork\_project

    $api->fork_project(
      $project_id,
    );

Sends a `POST` request to `/pojects/fork/:project_id`.

## delete\_project

    $api->delete_project(
      $project_id,
    );

Sends a `DELETE` request to `/projects/:project_id`. Returns true on
success.

## project\_members

    my $members = $api->project_members(
      $project_id,
      \%params,
    );

Sends a `GET` request to `/projects/:project_id/members` and returns the decoded/deserialized response body. Possible optional parameters are:

- `query`

## project\_member

    my $member = $api->project_member(
      $project_id,
      $user_id,
    );

Sends a `GET` request to `/project/:project_id/members/:user_id` and returns the decoded/deserialized response body.

## add\_project\_member

    $api->add_project_member(
      $project_id,
      \%params,
    );

Sends a `POST` request to `/projects/:project_id/members`. Possible parameters
are:

- `user_id` (required)
- `access_level` (required)

## edit\_project\_member

    $api->edit_project_member(
      $project_id,
      $user_id,
      \%params,
    );

Sends a `PUT` request to `/projects/:project_id/members/:user_id`. Possible
parameters are the same as those for `add_project_member`.

## remove\_project\_member

    $api->remove_project_member(
      $project_id,
      $user_id,
    );

Sends a `DELETE` request to `/projects/:project_id/members/:user_id`.

## project\_hooks

    my $hooks = $api->project_hooks(
      $project_id,
    );

Sends a `GET` request to `/projects/:project_id/hooks` and returns the decoded/deserialized response body.

## project\_hook

    my $hook = $api->project_hook(
      $project_id,
      $hook_id,
    );

Sends a `GET` request to `/project/:project_id/hooks/:hook_id` and returns the decoded/deserialized response body.

## create\_project\_hook

    $api->create_project_hook(
      $project_id,
      \%params,
    );

Sends a `POST` request to `/projects/:project_id/hooks`. Possible parameters
are:

- `url` (required)
- `push_events` (optional)
- `issues_events` (optional)
- `merge_request_events` (optional)
- `tag_push_events` (optional)

## edit\_project\_hook

    $api->edit_project_hook(
      $project_id,
      $hook_id,
      \%params,
    );

Sends a `PUT` request to `/projects/:project_id/hooks/:hook_id`. Possible
parameters are the same as those for `create_project_hook`.

## delete\_project\_hook

    my $hook = $api->delete_project_hook(
      $project_id,
      $hook_id,
    );

Sends a `DELETE` request to `/projects/:project_id/hooks/:hook_id` and returns the decoded/deserialized response body.

## set\_project\_fork

    $api->set_project_fork(
      $project_id,
      $forked_from_id,
    );

Sends a `POST` request to `/projects/:project_id/fork/:forked_from_id`.

## clear\_project\_fork

    $api->clear_project_fork(
      $project_id,
    );

Sends a `DELETE` request to `/projects/:project_id/fork`.

## search\_projects\_by\_name

    my $projects = $api->search_projects_by_name(
      $query,
      \%params,
    );

Sends a `GET` request to `/projects/search/:query` and returns the
decoded/deserialized response body. Possible parameters are:

- `query` (required)
- `per_page` (optional)
- `page` (optional)
- `order_by`=`id`, `name`, `created_at`, `last_activity_at` (optional)
- `sort`=`asc`, `desc` (optional)

# SNIPPET METHODS

See [http://doc.gitlab.com/ce/api/project\_snippets.html](http://doc.gitlab.com/ce/api/project_snippets.html).

## snippets

    my $snippets = $api->snippets(
      $project_id,
    );

Sends a `GET` request to `/projects/:project_id/snippets` and returns the decoded/deserialized response body.

## snippet

    my $snippet = $api->snippet(
      $project_id,
      $snippet_id,
    );

Sends a `GET` request to `/projects/:project_id/snippets/:snippet_id` and returns the decoded/deserialized response body.

## create\_snippet

    $api->create_snippet(
      $project_id,
      \%params,
    );

Sends a `POST` request to `/projects/:project_id/snippets`. Returns the
created snippet, as serialized data. Possible parameters are:

- `title` (required)
- `file_name` (required)
- `visibility_level` (required)
- `code` (required)

## edit\_snippet

    $api->edit_snippet(
      $project_id,
      $snippet_id,
      \%params,
    );

Sends a `PUT` request to `/projects/:project_id/snippets/:snippet_id`. 
Possible parameters are the same as those for `create_snippet`, but all are
optional.

## delete\_snippet

    my $snippet = $api->delete_snippet(
      $project_id,
      $snippet_id,
    );

Sends a `DELETE` request to `/projects/:project_id/snippets/:snippet_id` and returns the decoded/deserialized response body.

## raw\_snippet

    my $content = $api->raw_snippet(
      $project_id,
      $snippet_id,
    );

Sends a `GET` request to `/projects/:project_id/snippets/:snippet_id/raw` and returns the raw response body.

# REPOSITORY METHODS

See [http://doc.gitlab.com/ce/api/repositories.html](http://doc.gitlab.com/ce/api/repositories.html).

## tags

    my $tags = $api->tags(
      $project_id,
    );

Sends a `GET` request to `/projects/:project_id/repository/tags` and returns the decoded/deserialized response body.

## create\_tag

    $api->create_tag(
      $project_id,
      \%params,
    );

Sends a `POST` request to `/projects/:project_id/repository/tags`. Possible 
parameters are:

- `tag_name` (required)
- `ref` (required)
- `message` (optional)

## tree

    my $tree = $api->tree(
      $project_id,
      \%params,
    );

Sends a `GET` request to `/projects/:project_id/repository/tree` and returns the decoded/deserialized response body. Possible parameters are:

- `path` (optional)
- `ref_name` (optional)

## blob

    my $blob = $api->blob(
      $project_id,
      $commit_sha,
      \%params,
    );

Sends a `GET` request to 
`/projects/:project_id/repository/blobs/:commit_sha` and returns the response
body. Only possible parameter is:

- `filepath` (required)

## raw\_blob

    my $raw_blob = $api->raw_blob(
      $project_id,
      $blob_sha,
    );

Sends a `GET` request to 
`/projects/:project_id/repository/raw_blobs/:blob_sha` and returns the raw
response body.

## archive

    my $archive = $api->archive(
      $project_id,
      \%params,
    );

Sends a `GET` request to `/projects/:project_id/repository/archive` and 
returns the response body. Only possible parameter is:

- `sha` (optional)

## compare

    my $comparison = $api->compare(
      $project_id,
      \%params,
    );

Sends a `GET` request to `/projects/:project_id/repository/compare` and returns the decoded/deserialized response body. Possible parameters are:

- `from` (required)
- `to` (required)

## contributors

    my $contributors = $api->contributors(
      $project_id,
    );

Sends a `GET` request to `/projects/:project_id/repository/contributors` and returns the decoded/deserialized response body.

# FILE METHODS

See [http://doc.gitlab.com/ce/api/repository\_files.html](http://doc.gitlab.com/ce/api/repository_files.html).

## file

    my $file = $api->file(
      $project_id,
      \%params,
    );

Sends a `GET` request to `/projects/:project_id/repository/files` and returns the decoded/deserialized response body. Possible parameters are:

- `file_path` (required)
- `ref` (required)

## create\_file

    my $file = $api->create_file(
      $project_id,
      \%params,
    );

Sends a `POST` request to `/projects/:project_id/repository/files` and
returns the decoded/deserialized response body. Possible parameters are:

- `file_path` (required)
- `branch_name` (required)
- `commit_message` (required)
- `encoding` (optional)
- `content` (required)

## edit\_file

    my $file = $api->edit_file(
      $project_id,
      \%params,
    );

Sends a `PUT` request to `/projects/:project_id/repository/files` and
returns the decoded/deserialized response body. Possible parameters are the
same as those for `create_file`, but only `encoding` is optional.

## delete\_file

    my $file = $api->delete_file(
      $project_id,
      \%params,
    );

Sends a `DELETE` request to `/projects/:project_id/repository/files` and
returns the decoded/deserialized response body.
Possible parameters are:

- `file_path` (required)
- `branch_name` (required)
- `commit_message` (required)

# COMMIT METHODS

See [http://doc.gitlab.com/ce/api/commits.html](http://doc.gitlab.com/ce/api/commits.html).

## commits

    my $commits = $api->commits(
      $project_id,
      \%params,
    );

Sends a `GET` request to `/projects/:project_id/repository/commits` and returns the decoded/deserialized response body. Only possible parameter is:

- `ref_name` (optional)

## commit

    my $commit = $api->commit(
      $project_id,
      $commit_sha,
    );

Sends a `GET` request to `/projects/:project_id/repository/commits/:commit_sha` and returns the decoded/deserialized response body.

## commit\_diff

    my $diff = $api->commit_diff(
      $project_id,
      $commit_sha,
    );

Sends a `GET` request to `/projects/:project_id/repository/commits/:commit_sha/diff` and returns the decoded/deserialized response body.

## commit\_comments

    my $comments = $api->commit_comments(
      $project_id,
      $commit_sha,
    );

Sends a `GET` request to
`/projects/:project_id/repository/commits/:commit_sha/comments` and returns the 
decoded/deserialized response body.

## add\_commit\_comment

    $api->add_commit_comment(
      $project_id,
      $commit_sha,
      \%params,
    );

Sends a `POST` request to
`/projects/:project_id/repository/commits/:commit_sha/comments`. Possible
parameters are:

- `note` (required)
- `path` (optional)
- `line` (optional)
- `line_type`=`new`,`old` (optional)

# BRANCH METHODS

See [http://doc.gitlab.com/ce/api/branches.html](http://doc.gitlab.com/ce/api/branches.html).

## branches

    my $branches = $api->branches(
      $project_id,
    );

Sends a `GET` request to `/projects/:project_id/repository/branches` and returns the decoded/deserialized response body.

## branch

    my $branch = $api->branch(
      $project_id,
      $branch_name,
    );

Sends a `GET` request to
`/projects/:project_id/repository/branches/:branch_name` and returns the 
decoded/deserialized response body.

## protect\_branch

    $api->protect_branch(
      $project_id,
      $branch_name,
    );

Sends a `PUT` request to `/projects/:project_id/repository/branches/:branch_name/protect`.

## unprotect\_branch

    $api->unprotect_branch(
      $project_id,
      $branch_name,
    );

Sends a `PUT` request to
`/projects/:project_id/repository/branches/:branch_name/unprotect`.

## create\_branch

    my $branch = $api->create_branch(
      $project_id,
      \%params,
    );

Sends a `POST` request to `/projects/:project_id/repository/branches` and
returns the decoded/deserialized response body.

## delete\_branch

    $api->delete_branch(
      $project_id,
      $branch_name,
    );

Sends a `DELETE` request to
`/projects/:project_id/repository/branches/:branch_name`.

# MERGE REQUEST METHODS

See [http://doc.gitlab.com/ce/api/merge\_requests.html](http://doc.gitlab.com/ce/api/merge_requests.html).

## merge\_requests

    my $merge_requests = $api->merge_requests(
      $project_id,
    );

Sends a `GET` request to `/projects/:project_id/merge_requests` and returns the 
decoded/deserialized response body.

## merge\_request

    my $merge_request = $api->merge_request(
      $project_id,
      $merge_request_id,
    );

Sends a `GET` request to 
`/projects/:project_id/merge_request/:merge_request_id` and returns the 
decoded/deserialized response body.

## create\_merge\_request

    my $merge_request = $api->create_merge_request(
      $project_id,
      \%params,
    );

Sends a `POST` request to `/projects/:project_id/merge_requests` and returns 
the decoded/deserialized response body.

## edit\_merge\_request

    my $merge_request = $api->edit_merge_request(
      $project_id,
      $merge_request_id,
      \%params,
    );

Sends a `PUT` request to 
`/projects/:project_id/merge_requests/:merge_request_id` and returns the 
decoded/deserialized response body.

## accept\_merge\_request

    $api->accept_merge_request(
      $project_id,
      $merge_request_id,
      \%params,
    );

Sends a `PUT` request to 
`/projects/:project_id/merge_requests/:merge_request_id/merge`.

## add\_merge\_request\_comment

    $api->add_merge_request_comment(
      $project_id,
      $merge_request_id,
      \%params,
    );

Sends a `POST` request to 
`/projects/:project_id/merge_requests/:merge_request_id/comments`.

## merge\_request\_comments

    my $comments = $api->merge_request_comments(
      $project_id,
      $merge_request_id,
    );

Sends a `GET` request to
`/projects/:project_id/merge_requests/:merge_request_id/comments` and returns 
the decoded/deserialized response body.

# ISSUE METHODS

See [http://doc.gitlab.com/ce/api/issues.html](http://doc.gitlab.com/ce/api/issues.html).

## all\_issues

    my $issues = $api->all_issues(
      \%params,
    );

Sends a `GET` request to `/issues` and returns the decoded/deserialized 
response body.

## issues

    my $issues = $api->issues(
      $project_id,
      \%params,
    );

Sends a `GET` request to `/projects/:project_id/issues` and returns the decoded/deserialized response body.

## issue

    my $issue = $api->issue(
      $project_id,
      $issue_id,
    );

Sends a `GET` request to `/projects/:project_id/issues/:issue_id` and returns 
the decoded/deserialized response body.

## create\_issue

    my $issue = $api->create_issue(
      $project_id,
      \%params,
    );

Sends a `POST` request to `/projects/:project_id/issues` and returns the decoded/deserialized response body.

## edit\_issue

    my $issue = $api->edit_issue(
      $project_id,
      $issue_id,
      \%params,
    );

Sends a `PUT` request to `/projects/:project_id/issues/:issue_id` and returns 
the decoded/deserialized response body.

# LABEL METHODS

See [http://doc.gitlab.com/ce/api/labels.html](http://doc.gitlab.com/ce/api/labels.html).

## labels

    my $labels = $api->labels(
      $project_id,
    );

Sends a `GET` request to `/projects/:project_id/labels` and returns the decoded/deserialized response body.

## create\_label

    my $label = $api->create_label(
      $project_id,
      \%params,
    );

Sends a `POST` request to `/projects/:project_id/labels` and returns the 
decoded/deserialized response body.

## delete\_label

    $api->delete_label(
      $project_id,
      \%params,
    );

Sends a `DELETE` request to `/projects/:project_id/labels`.

## edit\_label

    my $label = $api->edit_label(
      $project_id,
      \%params,
    );

Sends a `PUT` request to `/projects/:project_id/labels` and returns the
decoded/deserialized response body.

# MILESTONE METHODS

See [http://doc.gitlab.com/ce/api/milestones.html](http://doc.gitlab.com/ce/api/milestones.html).

## milestones

    my $milestones = $api->milestones(
      $project_id,
    );

Sends a `GET` request to `/projects/:project_id/milestones` and returns the
decoded/deserialized response body.

## milestone

    my $milestone = $api->milestone(
      $project_id,
      $milestone_id,
    );

Sends a `GET` request to `/projects/:project_id/milestones/:milestone_id` and
returns the decoded/deserialized response body.

## create\_milestone

    $api->create_milestone(
      $project_id,
      \%params,
    );

Sends a `POST` request to `/projects/:project_id/milestones`.

## edit\_milestone

    $api->edit_milestone(
      $project_id,
      $milestone_id,
      \%params,
    );

Sends a `PUT` request to `/projects/:project_id/milestones/:milestone_id`.

# NOTE METHODS

See [http://doc.gitlab.com/ce/api/notes.html](http://doc.gitlab.com/ce/api/notes.html).

## notes

    my $notes = $api->notes(
      $project_id,
      $note_type,
      $merge_request_id,
    );

Sends a `GET` request to 
`/projects/:project_id/:note_type/:merge_request_id/notes` and returns the
decoded/deserialized response body.

## note

    my $note = $api->note(
      $project_id,
      $note_type,
      $merge_request_id,
      $note_id,
    );

Sends a `GET` request to
`/projects/:project_id/:note_type/:merge_request_id/notes/:note_id`
and returns the decoded/deserialized response body.

## create\_note

    $api->create_note(
      $project_id,
      $note_type,
      $merge_request_id,
      \%params,
    );

Sends a `POST` request to `/projects/:project_id/:note_type/:merge_request_id/notes`.

# DEPLOY KEY METHODS

See [http://doc.gitlab.com/ce/api/deploy\_keys.html](http://doc.gitlab.com/ce/api/deploy_keys.html).

## deploy\_keys

    my $keys = $api->deploy_keys(
      $project_id,
    );

Sends a `GET` request to `/projects/:project_id/keys` and returns the
decoded/deserialized response body.

## deploy\_key

    my $key = $api->deploy_key(
      $project_id,
      $key_id,
    );

Sends a `GET` request to `/projects/:project_id/keys/:key_id` and returns the decoded/deserialized response body.

## create\_deploy\_key

    $api->create_deploy_key(
      $project_id,
      \%params,
    );

Sends a `POST` request to `/projects/:project_id/keys`.

## delete\_deploy\_key

    $api->delete_deploy_key(
      $project_id,
      $key_id,
    );

Sends a `DELETE` request to `/projects/:project_id/keys/:key_id`.

# SYSTEM HOOK METHODS

See [http://doc.gitlab.com/ce/api/system\_hooks.html](http://doc.gitlab.com/ce/api/system_hooks.html).

## hooks

    my $hooks = $api->hooks();

Sends a `GET` request to `/hooks` and returns the decoded/deserialized
response body.

## create\_hook

    $api->create_hook(
      \%params,
    );

Sends a `POST` request to `/hooks`.

## test\_hook

    my $hook = $api->test_hook(
      $hook_id,
    );

Sends a `GET` request to `/hooks/:hook_id` and returns the 
decoded/deserialized response body.

## delete\_hook

    $api->delete_hook(
      $hook_id,
    );

Sends a `DELETE` request to `/hooks/:hook_id`.

# GROUP METHODS

See [http://doc.gitlab.com/ce/api/groups.html](http://doc.gitlab.com/ce/api/groups.html).

## groups

    my $groups = $api->groups();

Sends a `GET` request to `/groups` and returns the decoded/deserialized
response body.

## group

    my $group = $api->group(
      $group_id,
    );

Sends a `GET` request to `/groups/:group_id` and returns the
decoded/deserialized response body.

## create\_group

    $api->create_group(
      \%params,
    );

Sends a `POST` request to `/groups`.

## transfer\_project

    $api->transfer_project(
      $group_id,
      $project_id,
    );

Sends a `POST` request to `/groups/:group_id/projects/:project_id`.

## delete\_group

    $api->delete_group(
      $group_id,
    );

Sends a `DELETE` request to `/groups/:group_id`.

## group\_members

    my $members = $api->group_members(
      $group_id,
    );

Sends a `GET` request to `/groups/:group_id/members` and returns the
decoded/deserialized response body.

## add\_group\_member

    $api->add_group_member(
      $group_id,
      \%params,
    );

Sends a `POST` request to `/groups/:group_id/members`.

## remove\_group\_member

    $api->remove_group_member(
      $group_id,
      $user_id,
    );

Sends a `DELETE` request to `/groups/:group_id/members/:user_id`.

# SERVICE METHODS

See [http://doc.gitlab.com/ce/api/services.html](http://doc.gitlab.com/ce/api/services.html).

## edit\_project\_service

    $api->edit_project_service(
      $project_id,
      $service_name,
      \%params,
    );

Sends a `PUT` request to `/projects/:project_id/services/:service_name`.

## delete\_project\_service

    $api->delete_project_service(
      $project_id,
      $service_name,
    );

Sends a `DELETE` request to `/projects/:project_id/services/:service_name`.

# PRIVATE SUBROUTINES

The folowing subroutines are used by WWW::GitLab::v3 internally. Documentation
for these might disappear in the future, since nothing is expected to call these
other than WWW::GitLab::v3 itself.

## build\_url

    my $url = $api->build_url( \@members, \%params );

Generates a valid URI object from two arguments. The first is the reference to
an array of URI members, which will be passed to a URI constructor. The second
is a reference to a hash of values to be appended to the URI as parameters.

## \_serialize

    my $yaml = $api->_serialize( $api->_get($url) );

Wrapper function to JSON::Tiny. The argument is safely passed to this module's
`decode_json` function, and its result returned on success.

## \_get

    my $response = $api->_get( $url );

Performs a GET request on the provided URL using LWP::UserAgent. Returns the
response code on success.

## \_delete

    my $response = $api->_delete( $url );

Performs a DELETE request on the provided URL using LWP::UserAgent. Returns the
response code on success.

## \_put

    my $response = $api->_put( $url, \%form );
    my $response = $api->_put( $url, \@form );

Performs a PUT request on the provided URL using LWP::UserAgent. The form
argument is passed as-is to HTTP::Request::Common.

Returns the response code on success.

## \_post

    my $response = $api->_post( $url, \%form );
    my $response = $api->_post( $url, \@form );

Performs a POST request on the provided URL using LWP::UserAgent. The form
argument is passed as-is to HTTP::Request::Common.

Returns the response code on success.

# SEE ALSO

- [GitLab::API::v3](https://metacpan.org/pod/GitLab::API::v3), on which the interface is inspired
- [Net::Gitlab](https://metacpan.org/pod/Net::Gitlab), another possibly defunct API implementation

# AUTHOR

Jose Joaquin Atria, `<jjatria at gmail.com>`

# BUGS

Please report any bugs or feature requests to 
`bug-www-gitlab-v3 at rt.cpan.org`, or through the web interface at 
[http://rt.cpan.org/NoAuth/ReportBug.html?Queue=WWW-GitLab-v3](http://rt.cpan.org/NoAuth/ReportBug.html?Queue=WWW-GitLab-v3). I will be
notified, and then you'll automatically be notified of progress on your bug as I
make changes.

# SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc WWW::GitLab::v3

You can also look for information at:

- RT: CPAN's request tracker (report bugs here)

    [http://rt.cpan.org/NoAuth/Bugs.html?Dist=WWW-GitLab-v3](http://rt.cpan.org/NoAuth/Bugs.html?Dist=WWW-GitLab-v3)

- AnnoCPAN: Annotated CPAN documentation

    [http://annocpan.org/dist/WWW-GitLab-v3](http://annocpan.org/dist/WWW-GitLab-v3)

- CPAN Ratings

    [http://cpanratings.perl.org/d/WWW-GitLab-v3](http://cpanratings.perl.org/d/WWW-GitLab-v3)

- Search CPAN

    [http://search.cpan.org/dist/WWW-GitLab-v3/](http://search.cpan.org/dist/WWW-GitLab-v3/)

# LICENSE AND COPYRIGHT

Copyright 2015 J. Joaquin Atria.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

[http://www.perlfoundation.org/artistic\_license\_2\_0](http://www.perlfoundation.org/artistic_license_2_0)

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
